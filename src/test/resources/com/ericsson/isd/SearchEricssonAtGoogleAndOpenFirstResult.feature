Feature: Search ericsson site at Google and open the ericsson site
   As a user i want to search ericsson at google and open the ericsson site from results

@SetupDriver
Scenario: Search Ericcson in Google
   Given i open the explorer and navigate to "http://www.google.com"
   When i search "Ericsson" and press enter
   And i click on the link with text "Ericsson - A world of communication"
   Then i should be redirected to ericsson site
 